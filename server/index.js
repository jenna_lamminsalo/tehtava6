const mysql = require("mysql");
//const cors = require('cors');

const con = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "nakki",
  database: "Rest",
  multipleStatements: true, //out parametria varten aliohjelmassa
});

con.connect((err) => {
  if (err) {
    console.log("Error connecting to Db");
    return;
  }
  console.log("Connection established");
});

const express = require("express");
const bodyParser = require("body-parser");
const app = express().use(bodyParser.json());

// Add headers

app.use(function (req, res, next) {
  // Website you wish to allow to connect
  res.setHeader("Access-Control-Allow-Origin", "*");

  // Request methods you wish to allow
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, OPTIONS, PUT, PATCH, DELETE"
  );

  // Request headers you wish to allow
  res.setHeader(
    "Access-Control-Allow-Headers",
    "X-Requested-With,content-type"
  );

  // Set to true if you need the website to include cookies in the requests sent
  // to the API (e.g. in case you use sessions)
  res.setHeader("Access-Control-Allow-Credentials", true);

  // Pass to next layer of middleware
  next();
});

//GET all users
app.get("/users", (req, res) => {
  console.log("get users");
  con.query("SELECT * from urheilijat", function (err, rows) {
    if (err) throw err;
    console.log("Data received from Db:");
    console.log(rows);
    res.json(rows);
  });
});
//GET a user
app.get("/users/:id", (req, res) => {
  const id = Number(req.params.id);
  con.query(`SELECT * from urheilijat where id = ${id}`, (err, rows) => {
    if (err) throw err;
    console.log("Data received from Db:\n");
    console.log(rows[0]);
    const user = rows[0][0];
    res.json(user ? user : { message: "Not found" });
  });
});
//ADD a user
app.post("/users", (req, res) => {
  const user = req.body;
  const sukunimi = mysql.escape(user.sukunimi);
  const etunimi = mysql.escape(user.etunimi);
  const syntvuosi = mysql.escape(user.syntymavuosi);
  const paino = mysql.escape(user.paino);
  const kuvaUrl = mysql.escape(user.kuvaUrl);
  const laji = mysql.escape(user.laji);
  const saavutukset = mysql.escape(user.saavutukset);

  con.query(
    `INSERT INTO urheilijat (sukunimi, etunimi, syntymavuosi, paino, kuvaUrl, laji, saavutukset)
    VALUES (
        ${sukunimi}, 
        ${etunimi}, 
        ${syntvuosi}, 
        ${paino},
        ${kuvaUrl},
        ${laji}, 
        ${saavutukset}
    ); 
    `,
    (err, rows) => {
      if (err) throw err;

      console.log("Data received from Db:\n");
      console.log(rows);

      res.sendStatus(200);
    }
  );
});
//UPDATE a user
app.put("/users/:id", (req, res) => {
  const id = Number(req.params.id);
  const updatedUser = req.body;
  const sukunimi = mysql.escape(updatedUser.sukunimi);
  const etunimi = mysql.escape(updatedUser.etunimi);
  const syntvuosi = mysql.escape(updatedUser.syntymavuosi);
  const paino = mysql.escape(updatedUser.paino);
  const kuvaUrl = mysql.escape(updatedUser.kuvaUrl);
  const laji = mysql.escape(updatedUser.laji);
  const saavutukset = mysql.escape(updatedUser.saavutukset);

  con.query(
    `UPDATE urheilijat 
    SET 
    sukunimi = ${sukunimi},
    etunimi = ${etunimi},
    syntymavuosi = ${syntvuosi},
    paino = ${paino},
    laji = ${laji},
    kuvaUrl = ${kuvaUrl},
    saavutukset = ${saavutukset}
    WHERE id = ${id}`,
    (err, rows) => {
      if (err) throw err;

      res.sendStatus(200);
    }
  );
});
//DELETE a user
app.delete("/users/:id", (req, res) => {
  const id = Number(req.params.id);
  console.log("Delete user ", id);
  con.query(
    `DELETE FROM urheilijat 
    WHERE id = ${id}`,
    (err, rows) => {
      if (err) throw err;

      res.sendStatus(200);
    }
  );
});

console.log("starting server");
app.listen(3000, () => {
  console.log("ServerÂ listeningÂ atÂ portÂ 3000");
});
