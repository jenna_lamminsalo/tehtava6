//import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";

import { useState, useEffect } from "react";
import Axios from "axios";
import {
  Button,
  Card,
  ListGroup,
  ListGroupItem,
  Row,
  Container,
  Col,
  Form,
} from "react-bootstrap";

function App() {
  const [muokkaaId, setMuokkaaId] = useState(0);

  const [sukunimi, setSukunimi] = useState("");
  const [etunimi, setEtunimi] = useState("");
  const [syntymavuosi, setSyntvuosi] = useState("");
  const [paino, setPaino] = useState("");
  const [kuvaUrl, setKuva] = useState("");
  const [laji, setLaji] = useState("");
  const [saavutukset, setSaavutukset] = useState("");

  const [kaikkiUrheilijat, setUrheilijat] = useState([]);

  useEffect(() => {
    getUrheilijat();
  }, []);

  const lisaaUrheilija = () => {
    Axios.post("http://localhost:3000/users", {
      sukunimi: sukunimi,
      etunimi: etunimi,
      syntymavuosi: syntymavuosi,
      paino: paino,
      kuvaUrl: kuvaUrl,
      laji: laji,
      saavutukset: saavutukset,
    }).then(() => {
      console.log("success");
      lopetaMuokkaus();
      getUrheilijat();
    });
  };

  const updateUrheilija = () => {
    Axios.put(`http://localhost:3000/users/${muokkaaId}`, {
      sukunimi: sukunimi,
      etunimi: etunimi,
      syntymavuosi: syntymavuosi,
      paino: paino,
      kuvaUrl: kuvaUrl,
      laji: laji,
      saavutukset: saavutukset,
    }).then(() => {
      console.log("success");
      lopetaMuokkaus();
      getUrheilijat();
    });
  };

  const getUrheilijat = () => {
    Axios.get("http://localhost:3000/users").then((response) => {
      console.log(response);
      setUrheilijat(response.data);
    });
  };

  const deleteUrheilija = (id) => {
    Axios.delete(`http://localhost:3000/users/${id}`).then(() => {
      console.log("success");
      getUrheilijat();
    });
  };

  const editUrheilija = (user) => {
    console.log(user);
    setMuokkaaId(user.id);
    setSukunimi(user.sukunimi);
    setEtunimi(user.etunimi);
    setSyntvuosi(user.syntymavuosi);
    setPaino(user.paino);
    setLaji(user.laji);
    setSaavutukset(user.saavutukset);
    setKuva(user.kuvaUrl);
  };
  const lopetaMuokkaus = () => {
    setMuokkaaId(0);
    setSukunimi("");
    setEtunimi("");
    setSyntvuosi("");
    setPaino("");
    setLaji("");
    setSaavutukset("");
    setKuva("");
  };

  return (
    <Container className="App" fluid>
      <Row>
        <Col>
          <Container>
            {kaikkiUrheilijat.map((user, idx) => (
              <Row xs={1} md={2} className="g-4">
                <User
                  key={idx}
                  data={user}
                  onDelete={deleteUrheilija}
                  onEdit={editUrheilija}
                />
              </Row>
            ))}
          </Container>
        </Col>
        <Col>
          <Container>
            <Form>
              <Form.Group>
                <Form.Label>Sukunimi:</Form.Label>
                <Form.Control
                  type="text"
                  onChange={(event) => {
                    setSukunimi(event.target.value);
                  }}
                  value={sukunimi}
                />
              </Form.Group>
              <Form.Group>
                <Form.Label>Etunimi:</Form.Label>
                <Form.Control
                  type="text"
                  onChange={(event) => {
                    setEtunimi(event.target.value);
                  }}
                  value={etunimi}
                />
              </Form.Group>

              <Form.Group>
                <Form.Label>syntymavuosi:</Form.Label>
                <Form.Control
                  type="text"
                  onChange={(event) => {
                    setSyntvuosi(event.target.value);
                  }}
                  value={syntymavuosi}
                />
              </Form.Group>

              <Form.Group>
                <Form.Label>paino:</Form.Label>
                <Form.Control
                  type="number"
                  onChange={(event) => {
                    setPaino(event.target.value);
                  }}
                  value={paino}
                />
              </Form.Group>

              <Form.Group>
                <Form.Label>kuvaUrl:</Form.Label>
                <Form.Control
                  type="text"
                  onChange={(event) => {
                    setKuva(event.target.value);
                  }}
                  value={kuvaUrl}
                />
              </Form.Group>

              <Form.Group>
                <Form.Label>laji:</Form.Label>
                <Form.Control
                  type="text"
                  onChange={(event) => {
                    setLaji(event.target.value);
                  }}
                  value={laji}
                />
              </Form.Group>

              <Form.Group>
                <Form.Label>saavutukset</Form.Label>
                <Form.Control
                  type="text"
                  onChange={(event) => {
                    setSaavutukset(event.target.value);
                  }}
                  value={saavutukset}
                />
              </Form.Group>

              <Button
                type="submit"
                variant="primary"
                onClick={muokkaaId ? updateUrheilija : lisaaUrheilija}
              >
                {muokkaaId ? "Päivitä urheilija" : "Lisää urheilija"}
              </Button>
              {muokkaaId ? (
                <Button variant="danger" onClick={lopetaMuokkaus}>
                  Peruuta muokkaus
                </Button>
              ) : (
                <></>
              )}
            </Form>
          </Container>
        </Col>
      </Row>
    </Container>
  );
}

function User({ data, onDelete, onEdit }) {
  const user = data;

  const callDelete = () => onDelete(user.id);
  const callMuokkaa = () => onEdit(user);

  return (
    <>
      <Card>
        <Card.Img variant="top" src={user.kuvaUrl} height="128" width="128" />
        <Card.Body>
          <Card.Title>
            {user.etunimi} {user.sukunimi}
          </Card.Title>
          <Card.Text>
            <ListGroup>
              <ListGroupItem key="1">Paino {user.paino}</ListGroupItem>
              <ListGroupItem key="2">
                Syntymavuosi {user.syntymavuosi}
              </ListGroupItem>
              <ListGroupItem key="3">Laji {user.laji}</ListGroupItem>
              <ListGroupItem key="4">
                Saavutukset {user.saavutukset}
              </ListGroupItem>
            </ListGroup>
          </Card.Text>
          <Button variant="primary" onClick={callMuokkaa}>
            Muokkaa
          </Button>
          <Button variant="danger" onClick={callDelete}>
            Poista
          </Button>
        </Card.Body>
      </Card>
    </>
  );
}

export default App;
